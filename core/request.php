<?php

function app4legal_subscribe()
{
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $form = new app\controllers\FormRequests;
        $response = $form->subscribe($_POST);
        wp_send_json($response);
    }
}


function app4legal_contact_us()
{
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $form = new app\controllers\FormRequests;
        $response = $form->contactUs($_POST);
        wp_send_json($response);
    }
}


function app4legal_request_for_quote()
{
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $form = new app\controllers\FormRequests;
        $response = $form->RequestForQuote($_POST);
        wp_send_json($response);
    }
}