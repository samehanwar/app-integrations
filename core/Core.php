<?php

class Core 
{
    protected $config = [];
    
    protected $model;
    
    function __construct()
    {
        require __DIR__ . '/../config/config.php';
        $this->config = $config;
    }
    
    public function load($key)
    {
        return $this->config[$key];
    }
    
    public function config($key)
    {
        return $this->config[$key];
    }
    
    public function model($modelName)
    {
        if(isset($modelName)){
            $this->model = $modelName;
            require 'models/' . $this->model . '.php';
        }
    }
}