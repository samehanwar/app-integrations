<!doctype HTML>
<html>
    <head>
        <title> app4legal website | change base url </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php    
                        $siteLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $baseUrl = substr($siteLink, 0, strpos($siteLink, "base-url.php"));
                        echo '<div class="siteurl-wrapper">';
                        echo '<h1> update the database to fix the site url  </h1> <hr/>';
                        echo 'automatically: <input type="radio" name="updateSite" class="form-control" value="auto" checked="checked" />';
                        echo 'manually: <input type="radio" name="updateSite" class="form-control" value="man" />';
                        echo '<h3> your current site url is <p class="notice-message">'. $baseUrl .'</p>  <span> if it is not right, please put the link manually </span></h3>';
                        echo '<button class="btn btn-info" id="update-baseurl-btn"> update Base Url  </button>';
                        echo '<hr/><p> update the site url manually </p>';
                        echo '<input type="text" id="site-url" name="siteUrl" class="form-control" placeholder=" Your current site url " /> <button type="button" class="btn btn-info" id="update-baseurl-btn2"> update URL </button>';
                        echo '<div id="contact-loader" style="background: rgba(0,0,0,0.1);display: none;height: 100%;width:100%;position:fixed;z-index: 10000;top: 0;left: 0;"> <span id="loading5" style="top:40%;left: 40%;"><span id="outerCircle"></span></span></div> <div class="update-status"> </div>';
                        echo '</div>';
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"> </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"> </script>
<script type="text/javascript">
    $('#update-baseurl-btn,#update-baseurl-btn2').on('click',function(e){
            e.preventDefault();
            var currentUrl = window.location.href;
                if(e.target.id == 'update-baseurl-btn' && $('.siteurl-wrapper input[type="radio"]:checked').val() == 'auto'){
                    var baseurl = currentUrl.substring(0, currentUrl.indexOf('wp-content/'));
                }
                else{
                    baseurl = $('#site-url').val();
                }
                console.log(currentUrl + baseurl);
            $.ajax({
                url : baseurl+'wp-content/plugins/app4legal-integrations/index.php',
                type: 'POST',
                cache:false,
                data:{ baseurl : baseurl, action : 'update_siteurl' },
                beforeSend:function(){
                    $('#contact-loader').slideDown();
                },
                success:function( data ){
                    if(data == true){
                        $('#contact-loader').slideUp();
                        var message = 'all th e links updated successfully to the new siteurl';
                        $('.siteurl-wrapper .update-status').html("<p class='success'>" + message + "</p>");
                    }else{
                        $('#contact-loader').slideUp();
                        var message = 'something is going wrong, please try again';
                        $('.siteurl-wrapper .update-status').html("<p class='error'>" + message + "</p>");
                    }
                },
                error:function(){
                }
            });
            console.log('update me please for the base url' + baseurl );
        });
</script>