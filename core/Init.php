<?php

class Init
{

    public static function route($controllerName, $ajaxHook = null)
    {
        $functionName = function() use($controllerName)
        {
            $method =  (!empty($controllerName)) ? substr(strstr($controllerName, '->'), 2) : null;
            $controller =  (!empty($controllerName)) ? strstr($controllerName, '->', true) : null;
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $class = "app\\controllers\\" . $controller;
                $qualifiedControllerName = new $class; 
                $response = call_user_func_array([$qualifiedControllerName, $method], array($_POST));
            }
            else{
                $class = "app\\controllers\\" . $controller;
                $qualifiedControllerName = new $class; 
                $response = call_user_func_array([$qualifiedControllerName, $method], array());
            }
            return wp_send_json($response);
        };
        if($ajaxHook != null){
            $wp_actionHook  = add_action("wp_ajax_$ajaxHook", $functionName);
            $wp_actionHook .= add_action("wp_ajax_nopriv_$ajaxHook", $functionName);
            return  $functionName;
        }
//            return  $functionName;
    }
    
    public static function get($controllerName)
    {
        $method =  (!empty($controllerName)) ? substr(strstr($controllerName, '->'), 2) : null;
        $controller =  (!empty($controllerName)) ? strstr($controllerName, '->', true) : null;
        $class = "app\\controllers\\" . $controller;
        $qualifiedControllerName = new $class; 
        $response = call_user_func_array([$qualifiedControllerName, $method], array());
        return $response;
    }
    
    public static function RouteResponse($routeName)
    {
        if(!empty($routeName)){
            return self::get($routeName);
        }
        return false;
    }
    
    function isAjaxRequest() 
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 
            return true;
        }
        return false;
    }
}

//require  __DIR__ . '/DbTablesInstall.php';
//new DbTablesInstall();

