<?php 

class DbTablesInstall
{
    public function __construct() {
        return register_activation_hook(__FILE__, $this->app4legal_tables_install());
    }

    function app4legal_tables_install()
    {
        ob_start();
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        foreach (new DirectoryIterator(WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'app4legal-integrations' . DIRECTORY_SEPARATOR . 'wp-migrations') as $fileInfo) {
            if($fileInfo->isDot()) continue;
            if ($fileInfo->isFile() && $fileInfo->getExtension() === 'php') {
                include __DIR__ . DIRECTORY_SEPARATOR .'wp-migrations' . DIRECTORY_SEPARATOR . $fileInfo;
                $classname = trim($fileInfo->getBasename('.php'));
                $app = new $classname;
                $sql = $app->migrate();
                $sql .= $app->commit();
                dbDelta( $sql );
            }
        }
        ob_clean();
    }
}
    
    
