<?php
namespace app\models;

class ContactUs extends \app\library\Database
{
    protected $rules = [
                'required' => [ 
                    'firstName',
                    'lastName',
                    'email',
                    'phone',
                    'country',
                    'typeOfRequest',
                    'message' ],
                'email'    => 'email',
                'numeric'  => 'phone',
            ];
    
    protected $tableName = 'ap4l_site_requests';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function validateData($data)
    {
        $validation = new \app\helpers\Validation();
        $response = $validation->validate($data, $this->rules);
        return $response;
    }
    
    public function insert($table, $fields) {
        $date = date("Y-m-d H:i:s");
        $query = 'INSERT INTO '. $table .'  VALUES("null","'.$fields["firstName"].'","'.$fields["lastName"].'",'
                . '"'.$fields["organization"].'","'.$fields["email"].'","'.$fields["phone"].'","'.$fields["country"].'",'
                . '"'.$fields["typeOfRequest"].'","'.$fields["message"].'","'.$fields["haveApp4legal"].'","null","null","null","'.$date.'")';
        return $this->db->query( $query );
    }
}