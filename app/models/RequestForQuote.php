<?php
namespace app\models;

class RequestForQuote extends \app\library\Database
{
    protected $rules = [
                'required' => [ 
                    'firstName',
                    'lastName',
                    'organization',
                    'email',
                    'phone',
                    'country',
                    'installation',
                    'training',
                    'noOfUsers',
                    'message' ],
                'email'    => 'email',
                'numeric'  => 'phone',
            ];
    
    protected $tableName = 'ap4l_site_requests';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function validateData($data)
    {
        $validation = new \app\helpers\Validation();
        $response = $validation->validate($data, $this->rules);
        return $response;
    }
    
    public function insert($fields)
    {
        $date = date("Y-m-d H:i:s");
        $query = 'INSERT INTO '. $this->tableName .'  VALUES("null","'.$fields["firstName"].'","'.$fields["lastName"].'",'
                . '"'.$fields["organization"].'","'.$fields["email"].'","'.$fields["phone"].'","'.$fields["country"].'",'
                . '"null","'.$fields["message"].'","null","'.$fields["training"].'","'.$fields["installation"].'","'.$fields["noOfUsers"].'","'.$date.'")';
        return $this->db->query( $query );
    }
}