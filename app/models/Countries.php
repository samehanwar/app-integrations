<?php
namespace app\models;

class Countries extends \app\library\Database
{
    protected $tablename = 'ap4l_countries';
        
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getCountryByName($countryName)
    {
        $query = 'SELECT id FROM ' . $this->tablename . ' WHERE countryName = "' . $countryName . '" LIMIT 1 ';
        $countryId = $this->db->get_results($query, OBJECT);
        if(!empty($countryId)){
            return $countryId[0]->id;
        }
    }
}
