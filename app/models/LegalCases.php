<?php
namespace app\models;

class LegalCases
{
    protected $api;
    
    public function __construct()
    {
        $this->api = new \app\helpers\Api();
    }

    public function addLegalCase($data)
    {
        $url = '/cases/add_legal_matter/';
        return $this->api->initApi($url, $data);
    }
    
    public function addRelatedContact($data)
    {
        $url = '/cases/related_contacts_add';
        return $this->api->initApi($url, $data);
    }
    
    public function addRelatedCompany($data)
    {
        $url = '/cases/related_companies_add/';
        return $this->api->initApi($url, $data);
    }
 
}