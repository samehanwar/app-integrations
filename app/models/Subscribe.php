<?php
namespace app\models;

class Subscribe extends \app\library\Database
{
    protected $rules = [
                'required' =>  'email' 
            ];
    
    protected $tableName = 'ap4l_site_requests';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function validateData($data)
    {
        $validation = new \app\helpers\Validation();
        $response = $validation->validate($data, $this->rules);
        return $response;
    }
    
    public function insert($fields)
    {
        $date = date("Y-m-d H:i:s");
        $query = 'INSERT INTO '. $this->tableName .'(email)  VALUES("'.$fields["email"].'")';
        return $this->db->query( $query );
    }
}