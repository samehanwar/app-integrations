<?php
namespace app\models;

class Client
{
    protected $api;
    
    public function __construct()
    {
        $this->api = new \app\helpers\Api();
    }

    public function addClient($data)
    {
        $url = '/clients/add/';
        return $this->api->initApi($url, $data);
    }
    
    public function checkIfClientExists($data)
    {
        $url = '/clients/check_if_exists/';
        return $this->api->initApi($url, $data);
    }
    
}