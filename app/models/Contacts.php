<?php
namespace app\models;

class Contacts
{
    protected $api;
    
    public function __construct()
    {
        $this->api = new \app\helpers\Api();
    }

    public function addContact($data)
    {
        $url = '/contacts/add/';
        return $this->api->initApi($url, $data);
    }
    
    public function addCompanyContact($data)
    {
        $url = '/contacts/edit/';
        return $this->api->initApi($url, $data);
    }

    public function checkIfContactExists($data)
    {
        $url = '/contacts/check_if_exists/';
        return $this->api->initApi($url, $data);
    }
    
}