<?php
namespace app\models;

class Company
{
    protected $api;
    
    public function __construct()
    {
        $this->api = new \app\helpers\Api();
    }

    public function addCompany($data)
    {
        $url = '/companies/add/';
        return $this->api->initApi($url, $data);
    }
    
    public function checkIfCompanyExists($data)
    {
        $url = '/companies/check_if_exists/';
        return $this->api->initApi($url, $data);
    }
    
}