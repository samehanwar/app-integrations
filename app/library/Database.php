<?php
namespace app\library;

class Database
{
    protected $db;
    
    protected $tableName;

    public function __construct() {
        $conifg = new \Core();
        try {
            $this->db = new \wpdb($conifg->load('db_user'), $conifg->load('db_pass'), $conifg->load('db_name'), $conifg->load('db_host'));
            throw new \Exception($this->db->show_errors());
        } catch (\Exception $e) {    
            echo $e->getMessage();
        }
    }

    public function getAll() {
        $query = "SELECT * FROM  " . $this->tableName . " ";
        return $this->db->get_results( $query, OBJECT );
    }
    
    
}