<?php
namespace app\library;

class DbMigrations 
{
    
    protected $tableName;
    protected $coulmn;
    protected $sqlStatment;
    protected $charSet = 'utf8mb4';
    protected $primaryKey;
    protected $foreignKey;


    protected function create()
    {
        $sql  = "";
        $sql .= "CREATE TABLE  " .$this->tableName . " (" . "\n";
        $sql .= $this->coulmn;
        !empty($this->primaryKey) ? $sql .= $this->primaryKey : '';
        $sql .= ");";
        return $this->sqlStatment = $this->cleanSqlStatment($sql);
    }
    
    protected function run($query)
    {
        if(!empty($query)){
            return  $query;
        }
        return false;
    }

    protected function coulmn($coulmnName, array $dataType)
    {
        if(!empty($dataType) && is_array($dataType)){
            foreach ($dataType as $item){
                $string[] = $item ;
            }
            $this->coulmn .=  $coulmnName . ' ' . implode(' ', $string) . ',' . "\n";
            return $this;
        }
        return false;
    }
    
    protected function primaryKey($primarykey)
    {
        if(!empty($primarykey)){
            $primarykey = 'PRIMARY KEY ' . '(' . $primarykey. ')' . ',' . "\n";
            $this->primaryKey .= $primarykey;
        return $this;
        }
        return false;
    }
    
    protected function foreignKey($foreignkey, $refrence)
    {
        if(!empty($foreignkey) && !empty($refrence)){
            $foreignkey = 'FOREIGN KEY ' . '(' . $foreignkey. ') ' . "REFERENCES" . $refrence . ' (' . $foreignkey. ')'. ',' . "\n" ;
            $this->foreignKey .= $foreignkey;
        return $this;
        }
        return false;
    }
    
    protected function table($tableName)
    {
        if(!empty($tableName)){
            $this->tableName = $tableName;
            return $this;
        }
        return false;
    }
    
    private function cleanSqlStatment($sql)
    {
        return str_replace("),\n)", ")\n)", $sql);
    }
}