<?php
namespace app\helpers;

class AppControlCenter
{

    public $ccPath = null;
    public $tokenPath = null;
    
    public function __construct()
    {
        $core = new \Core();
        $this->ccPath = $core->config('cc_url');
        $this->tokenPath = '';
    }

    /**
     * Get Items, Packges, Items values per package, prices for each package relative to client IP
     * If IP don't match any Saved Category, first one in the DB will be the default
     */
    public function getPackagesPricesByIP()
    {
        $client = new \app\helpers\Client();
        
        $url = $this->ccPath . '/api/getPackagesPricesByIP/';
        $fields = ['ip' => $client->getIP(), 'token' => file_get_contents($this->tokenPath)];

        $postFields = '';
        foreach ($fields as $key => $value) {
            $postFields .= $key . '=' . $value . '&';
        }
        rtrim($postFields, '&');
       
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => count($fields),
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
