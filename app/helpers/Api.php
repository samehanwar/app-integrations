<?php
namespace app\helpers;

class Api 
{
    protected $client;
    protected $config = [];
    private $apiKey;
    private $apiUrl;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client(
                        array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false ),
                               'verify' => false)
                        );
        $this->config = new \Core();
        $this->apiUrl = $this->config->load('api_uri');
        $this->apiKey = $this->config->load('api_key');
    }
    
    public function initApi($url,$data)
    {
        $response = $this->client->request('POST',
            $this->apiUrl.$url, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'x-api-key'    => $this->apiKey,
            ],
            'form_params' => $data
        ]);
        $jsonResponse = json_decode($response->getBody(), true);
        $jsonResponse['success'] ? $result = $jsonResponse['success'] : false;
        return $result;
    }
    
}