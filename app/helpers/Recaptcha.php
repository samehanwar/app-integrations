<?php
namespace app\helpers;

class Recaptcha 
{
    protected $recaptcha;
    protected $secret;

    function __construct() 
    {
        $this->secret = $this->getSecretKey();
        $this->recaptcha = new \ReCaptcha\ReCaptcha($this->secret);
    }
    
    public function validateCaptcha($data)
    {
        $resp = $this->recaptcha->verify($data, $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess()){
            $response['success'] = true;
        }
        else{
            $response['errors'] = $resp->getErrorCodes();
        }
        return $response;
    }
    
    private function getSecretKey()
    {
        $config = new \Core();
        return $config->load('captcha_secret');
    }

}