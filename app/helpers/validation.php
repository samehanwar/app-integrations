<?php
namespace app\helpers;

use Valitron;

class Validation 
{
    
    protected $fields = [];
    protected $rules  = [];

    public function validate($fields,$rules)
    {
        
        $validation = new Valitron\Validator($fields);
        $validation->rules($rules);
        
        if($validation->validate()) {
            $response['success'] = true;
        } else {
            $response['errors'] = $validation->errors();
        }
        return $response;
    }
    
}