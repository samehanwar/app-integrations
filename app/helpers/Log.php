<?php
namespace app\helpers;

class Log 
{
    protected $logFile ;
    protected $path ;
            
    function __construct($file)
    {
        $this->path = $this->getPath();
        if (!is_dir($this->path)){ 
            mkdir($this->path,0777);
        }
	$this->logFile = $this->path.$file;
    }    
    
    public function logData($data)
    {
        $log  = date('r') . '  -  '. $_SERVER['REMOTE_ADDR'] . '  -  ';
        $log .= json_encode($data) . PHP_EOL;
        file_put_contents($this->logFile, $log, FILE_APPEND);
        return true;
    }
    
    public function getPath()
    {
        $config = new \Core();
        if(empty($config->load('log_path'))){
            $path = WP_PLUGIN_DIR .'/app4legal-integrations/log/';
        }
        else{
            (substr(trim($config->load('log_path')), -1) == "/") ? 
                    $path = $config->load('log_path') : 
                    $path = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . $config->load('log_path')."/";
        }
        return $this->path = $path;
    }

}