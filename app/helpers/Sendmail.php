<?php
namespace app\helpers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email 
{
    protected $content;
    
    public function sendEmail($data, $recepientTitle = null, $clientType = null, $isExists = false, $template = null)
    {
        $config = new \Core();
        $mail = new PHPMailer(true);                             
        try {
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //Server settings
        $mail->SMTPDebug = 3;                                
        $mail->isSMTP();                                     
        $mail->Host = $config->load('mail_host');  
        $mail->SMTPAuth = true;                              
        $mail->Username = $config->load('mail_username');                 
        $mail->Password = $config->load('mail_password');                     
        $mail->SMTPSecure = $config->load('mail_smtp_secure');                            
        $mail->Port = $config->load('mail_port');                                    

        $mail->setFrom($config->load('mail_username'), 'App4Legal Team');
        $mail->addAddress($config->load('email_to'), 'App4Legal Team'); 
        
        //Content
        $mail->isHTML(true);                          
        if($isExists == true){
            $mail->Subject = date('Y/m/d', time()) . ' ISCRM Data Exists - App4Legal( ' . $data['nameOfRequest'] . ')';
            $mail->Body    = $this->userExistsMessage($data, $recepientTitle, $clientType);
        }elseif($template == true){
            $mail->Subject = 'App4Legal - Compalete your registeration for a Free demo';
            $mail->Body    = $template;
        }else{
            $mail->Subject = 'Request from ' . $data['firstName'] .' ' . $data['lastName'] . ' via App4Legal website';
            $mail->Body    = $this->MessageContent($data, $recepientTitle);
        }

        $mail->send();
            return $response['success'] =  'Message has been sent';
        } catch (\Exception $e) {
            return $response['error'] = 'Message could not be sent.';
        }
    }
    
    public function MessageContent($data, $recepientTitle)
    {
        $message  = '<p> Dear Support Team,  </p>';
        $message .= '<h3> '. $data['nameOfRequest'] . '  -  '. $recepientTitle . ' </h3>';
        $message .= '<table border="1" style="border:1px solid #f7f7f7;padding:30px;">
                    <tr> <td colspan="2"></td></tr>';
                foreach($data as $label => $value){
                    $message .=  '<tr><td><strong>'. $this->readableText($label) .'</strong></td> <td>'. $value .'</td></tr>';
                }
        $message .= '<tr style="background: #f7f7f7;"> <td colspan="2"> </td></tr></table>';
        return $message;
    }
    
    public function userExistsMessage($data, $recepientTitle, $clientType)
    {
        if ($recepientTitle != null && $clientType == 'contact') {
            $message = 'Contact (' . $data['firstName'] . ' ' . $data["lastName"] . ') exists in ISCRM. <br />';
        } else {
            if ($recepientTitle != null && $clientType == 'company') {
                $message = 'Information about (' . $data['organization'] . ') already exists in ISCRM.';
            }
        }
        return $message;
    }
    
    private function readableText($label)
    {
        $text = str_split($label);
        foreach($text as $char){
            if(ctype_upper($char)){
                $char = " " . strtolower($char);
            }
            $string[] = $char;
        }
        return $str = implode('', $string);
    }

}