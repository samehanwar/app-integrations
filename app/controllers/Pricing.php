<?php
namespace app\controllers;

use app\helpers\AppControlCenter;

class Pricing 
{

    public function getPricingData()
    {
        $appCC = new AppControlCenter();
        $pricingData = $appCC->getPackagesPricesByIP();
        return  $pricingData['success']['data'];
        
    }
}