<?php
namespace app\controllers;

Class FormRequests extends FormHandler
{
    public function RequestForQuote($data)
    {       
        if(!empty($data)){
            $data['nameOfRequest'] = 'Request For Quote';
            $response = $this->formManipulate($data);
            return $response;
        }
    }
    
    public function contactUs($data)
    {       
        if(!empty($data)){
            $data['nameOfRequest'] = 'Contact Us';
            $response = $this->formManipulate($data);
            return $response;
        }
    }
    
    public function subscribe($data)
    {
        if(!empty($data)){
            $data['nameOfRequest'] = 'Subscribe';
            $response = $this->subscribeManipulate($data);
            return $response;
        }
    }
}