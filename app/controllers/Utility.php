<?php
namespace app\controllers;

use app\library\Database;

class Utility extends Database
{
    public function __construct()
    {
        parent::__construct();
    }

    public function updateBaseUrl($data)
    {
        $tableName = 'wp_options';
        $query = "SELECT option_id, option_name, option_value FROM " . $tableName . " ORDER BY option_id ASC  LIMIT 10";
        $results = $this->db->get_results($query, OBJECT);
        foreach ($results as $row){
            if($row->option_name == 'siteurl'){
                $currentSiteUrl =  $row->option_value;
            } 
        }        
        
        $newbaseUrl = $data['baseurl'];        
        $queryReplace = "UPDATE wp_posts, wp_options  SET wp_posts.guid = REPLACE(wp_posts.guid, '$currentSiteUrl', '$newbaseUrl'), wp_options.option_value = REPLACE(wp_options.option_value, '$currentSiteUrl', '$newbaseUrl') "
                      . " WHERE wp_posts.guid LIKE '%$currentSiteUrl%' OR wp_options.option_value LIKE '%$currentSiteUrl%'";
        $replaceResult = $this->db->query($queryReplace);
        if($replaceResult){
            return true;
        }
    }
    
}

