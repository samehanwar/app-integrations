<?php
namespace app\controllers;

use app\library\Controller;

class FormHandler extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    
    public function formManipulate($data)
    {       
            
            isset($data['captcha']) ? $captchaCode = $data['captcha'] : '';
            // remove unneccessary array elements to be logged
            unset($data['captcha'], $data['action']);
            // log post data
            $log = new \app\helpers\Log('siteRequests.log');
            $log->logData($data);
            
            // validate post data
            if(!empty($data['nameOfRequest'])){
                switch ($data['nameOfRequest']) {
                    case 'Request For Quote':
                        $request = new \app\models\RequestForQuote();
                        break;
                    case 'Contact Us':
                        $request = new \app\models\ContactUs();
                        break;
                    default:
                        $request = new \app\models\RequestForQuote();
                }
            }
            $response = $request->validateData($data);
            
            if($response['success'] == true && !empty($response['success'])){
                $response = true;
                $log->logData('the validation passes successfully');
                // validate the captcha 
                $captcha = new \app\helpers\Recaptcha();
                $result = $captcha->validateCaptcha($captchaCode);
                if(!empty($result['errors'])){
                    $captchaResponse['errors']['g-recaptcha-response'][0] = 'the captcha code is wrong';
                    $log->logData('the captcha code is wrong');
                    return $captchaResponse ;
                }
                
                $log->logData('the captcha is validated');
                // insert a legal matter 
                $country = new \app\models\Countries();
                $countryId = $country->getCountryByName($data['country']);
                
                $company = new \app\models\Company();
                $companyName = ['name' => $data['organization']];
                $response = $company->checkIfCompanyExists($companyName);
                
                if(!empty($response['data'])){
                    $newcompany = false;
                    $companyId = $response['data'][0]['id'];
                    $companyresponse = $data['organization'] . ' - the company is already exits';
                }
                else{
                    $companyData = ['name' => $data['organization'], 'shortName' => substr($data['organization'], 0, 15), 'nationality_id' => $countryId];
                    $response = $company->addCompany($companyData);
                    if(!empty($response['data'])){
                        $newcompany = true;
                        $companyId = str_replace('COM', '', $response['data']['company_id']);
                        $companyresponse = 'added new company with id ' . $companyId;
                    }
                }
                $log->logData($companyresponse);
                
                $contact = new \app\models\Contacts();
                $email = ['email' => $data['email']];
                $response = $contact->checkIfContactExists($email);
                if(!empty($response['data'])){
                    $newcontact = false;
                    $contactId = $response['data'][0]['id'];
                    $log->logData('the contact with ' . $contactId . ' id  is exists');
                }
                else{
                    $newcontact = true;
                    $formData = [
                        'firstName' => $data['firstName'],
                        'lastName'  => $data['lastName'],
                        'email' => $data['email'],
                        'phone' => $data['phone']
                    ];
                    $response = $contact->addContact($formData);
                    if(!empty($response['data'])){
                        $contactId = $response['data']['contact_id'];
                        $log->logData('new contact with ' . $contactId . ' id  is created');
                    }
                }
                
                if($contactId && $companyId){
                    $companyContact = [
                        'id' => $contactId,
                        'firstName' => $data['firstName'],
                        'lastName'  => $data['lastName'],
                        'companies_contacts' => [$companyId]
                    ];
                    $response = $contact->addCompanyContact($companyContact);
                    $log->logData('new contact with ' . $contactId . ' added to the company ' . $companyId );
                }
                
                // add Client 
                $client = new \app\models\Client();
                $accountId = ['company_id' => $companyId];
                $response = $client->checkIfClientExists($accountId);
                if(empty($response['data'])){
                   $response = $client->addClient($accountId);
                   if(!empty($response['data'])){
                       $clientId = $response['data'][0]['id'];
                       $log->logData('new client added with ' . $clientId );
                   }else{
                       $log->logData('failed to add a new client');
                   }
                }

                $description = '';
                foreach ($data as $key => $value) {
                    $description .= $key . ": " . $value . "\n";
                }
                $description .= "website page: " . $data['nameOfRequest'];
                $legalCase = new \app\models\LegalCases();
                $legalMatter = [ 
                    'subject' => "[" . $data['organization'] . " - " . $data['country'] . "] App4Legal " . $data['nameOfRequest'],
                    'description' => $description,
                    'priority' => $this->config->load('matter_priority'),
                    'externalizeLawyers' => $this->config->load('matter_externalizeLawyers'),
                    'provider_group_id'  => $this->config->load('matter_provider_group_id'),
                    'assignedTeam' => $this->config->load('matter_assignedTeam'),
                    'assignee' => $this->config->load('matter_assignee'),
                    'requestedBy' => $contactId,
                    'case_type_id' => $this->config->load('matter_case_type_id'),
                    'arrivalDate' => date("Y-m-d H:i:s", time()),
                    'client_id' => $clientId
                ];
                $response = $legalCase->addLegalCase($legalMatter);
                if(!empty($response['data'])){
                    $log->logData('add new legal matter');
                    $legalCaseId = str_replace('M', '', $response['data']['case_id']);
                    $relatedContact = $legalCase->addRelatedContact(['case_id' => $legalCaseId, 'contact_id' => $contactId]);
                    if(empty($relatedContact)){
                        $log->logData('add related contact');
                    }
                    $relatedCompany = $legalCase->addRelatedCompany(['case_id' => $legalCaseId, 'company_id' => $companyId]);
                    if(empty($relatedCompany)){
                        $log->logData('add related company');
                    }
                }else{
                    $log->logData('can not create a new legal matter');
                }
                // save data to internal database
                $queryResult = $request->insert($data);
                if(!empty($queryResult) && $queryResult == true){
                    $queryResultStatus = 'data inserted into local database successfully';
                }else{
                    $queryResultStatus = 'failed insert into local database';
                }
                $log->logData($queryResultStatus);
                
                //send an email to the applied user 
                $sendemail = new \app\helpers\Email();
                
                if($newcompany == false){
                    $emailResponse = $sendemail->sendEmail($data, $companyId, 'company', true);
                    $emailResponse .= " For existing Company - " . $companyId;
                    $log->logData($emailResponse);
                }
                if($newcontact == false){
                    $emailResponse  = $sendemail->sendEmail($data, $contactId, 'contact', true);
                    $emailResponse .= " For existing Contact - " . $contactId;
                    $log->logData($emailResponse);
                }
                $emailResponse = $sendemail->sendEmail($data);
                $log->logData($emailResponse);
                return $response;
            }
            else{
                $log->logData("validation didn\'t passes");
                return $response;
            }
    }
    
    public function subscribeManipulate($data)
    {
            unset($data['action']);
            $log = new \app\helpers\Log('siteRequests.log');
            $log->logData($data);
            
            // validate post data
            $request = new \app\models\Subscribe();
            $response = $request->validateData($data);
            if($response['success'] == true && !empty($response['success'])){
              
                $log->logData('the validation passes successfully');
                
                $queryResult = $request->insert($data);
                if(!empty($queryResult) && $queryResult == true){
                    $queryResultStatus = 'data inserted into local database successfully';
                }else{
                    $queryResultStatus = 'failed insert into local database';
                }
                $log->logData($queryResultStatus);
                //send an email to the applied user 

                $sendemail = new \app\helpers\Email();
                
                $template = "thank you for choosing us please click the link below to complete your registeration <br/> <a href='https://www.app4legal.com/app4legal-cc/public/app/signup'> complete your registeration </a>";
                
                $emailResponse = $sendemail->sendEmail($data, false, false, false, $template);
                $log->logData($emailResponse);
                return $response;
            }
            else{
                $log->logData("validation didn\'t passes for subscribe");
                return $response;
            }
    }
}
