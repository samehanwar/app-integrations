<?php
class Migrate_site_requests extends app\library\DbMigrations
{
    public function migrate()
    {
        $table = $this->table('ap4l_site_requests');
        $table->coulmn('id', ['mediumint(9)', 'NOT NULL' ,'AUTO_INCREMENT'])
              ->coulmn('firstname', ['varchar(55)', 'NOT NULL'])
              ->coulmn('lastname', ['varchar(55)', 'NOT NULL'])
              ->coulmn('organization', ['varchar(55)', 'NOT NULL'])
              ->coulmn('email', ['varchar(55)'])
              ->coulmn('phone', ['varchar(55)'])
              ->coulmn('country', ['varchar(100)'])
              ->coulmn('request_type', ['varchar(100)'])
              ->coulmn('message', ['TEXT'])
              ->coulmn('have_instance', ['char(1)'])
              ->coulmn('training', ['varchar(100)'])
              ->coulmn('installation_type', ['varchar(100)'])
              ->coulmn('no_of_users', ['varchar(100)'])
              ->coulmn('time_created', ['datetime', 'DEFAULT CURRENT_TIMESTAMP'])
              ->primaryKey('id');
        return $this->create();
    }
    
    public function commit()
    {
        
    }
    
}

