<?php

//  database config data
$config['db_host'] = 'localhost';
$config['db_user'] = 'root';
$config['db_pass'] = 'mysql';
$config['db_name'] = 'app4legal-wp';


// RECAPTCHA SECRET KEYS
$config['captcha_secret'] = '6Le37xUUAAAAAM50Xk9dJRICXQsl0gX-DLzGa66V';
$config['captcha_public'] = '6Le37xUUAAAAAGtO-cijseNUlGt1Xmz1HETlWvKX';


// EMAIL SETTINGS
$config['mail_host'] = 'app4legal.com';
$config['mail_username'] = 'support@app4legal.com';
$config['mail_password'] = '';
$config['mail_smtp_secure'] = '';
$config['mail_port'] = 25;

// EMAIL TO
$config['email_to'] = '';

/*
 * LOG directory  path
 * if it is not defined it will set to default value 
 * @default value : pluginfolder / log DIR .
 * 
 */

// LOG 
$config['log_path'] = 'app4legal-integrations/log';


// SSO
$config['signin'] = 'https://www.app4legal.com/app4legal-cc/public/app/signin';
$config['signup'] = 'https://www.app4legal.com/app4legal-cc/public/app/signup';

//API
$config['api_uri'] = 'https://app4legal.com/site/2010/modules/api' ;
$config['api_key'] = '0b31e252fdcf76f3a6f89fbbda2adb37';


// LEGAL MATTER
$config['matter_priority'] = 'critical';
$config['matter_externalizeLawyers'] = 'yes';
$config['matter_provider_group_id'] = 1;
$config['matter_case_type_id'] = 1;
$config['matter_caseStage'] = 3;
$config['matter_assignedTeam'] = 2;
$config['matter_assignee'] = 0000000002;