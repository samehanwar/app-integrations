jQuery(document).ready(function($){
    
    $('#subscribe-button').on('click',function(e){
        e.preventDefault();
        var subscribeField = $('#subscribe').val();
        if(!validateEmail(subscribeField)){
            var message = 'the email you entered is invalid , try a gain with avalid one';
            $(this).after(showModal(message, 'error'));
            $('.ap4l-subscription-modal-lg').modal();
        }else{
            $.ajax({
                url : apl4_signup.ajax_url,
                type: 'POST',
                cache:false,
                data:{ email : subscribeField, action : 'manipulate_subscribe' },
                beforeSend:function(){
                    $('#contact-loader').slideDown();
                },
                success:function( data ){
                    if(data.errors){
                        $('#contact-loader').slideUp();
                        var message = 'something is going wrong , plese try later';
                        $('#subscribe-button').after(showModal(message, 'error'));
                        $('.ap4l-subscription-modal-lg').modal();
                    }else{
                        $('#contact-loader').slideUp();
                        var message = 'please check your email, <br/> we have sent a link while we preparing you free demo ';
                        $('#subscribe-button').after(showModal(message, 'success'));
                        $('.ap4l-subscription-modal-lg').modal();
                    }
                },
                error:function(){
                }
            });
        }
    });
    
    $('#contact-button').on('click',function(e){
        e.preventDefault();
        var firstname = $('#firstName').val(),
            lastname = $('#lastName').val(),
            email = $('#email').val(),
            organization = $('#organization').val(),
            phone = $('#phone').val(),
            country = $('#country').val(),
            typeOfRequest = $('#request-quote').val(),
            haveInstance = $('#have-instance').val(),
            captcha = $('.g-recaptcha-response').val(),
            message = $('#message').val();
        var formdata = $('#contact-form').serialize();
        $.ajax({
           url : apl4_signup.ajax_url,
           type: 'POST',
           cache:false,
           data:{ firstName : firstname, lastName : lastname, email : email, organization : organization,captcha:captcha, phone: phone, country: country, typeOfRequest: typeOfRequest, haveInstance: haveInstance, message:message, action : 'manipulate_contact_us' },
           beforeSend:function(){
               $('#contact-loader').slideDown();
           },
           success:function( data ){
               console.log(data);
               $('p.text-error').remove();
               if(data.errors){
                    $('#contact-loader').slideUp();
                    $('#contact-form').find('input,textarea,select').each(function(index,ele){
                        $(this).after('<p class="text-error" style="color:crimson;margin-top:-5px;"> </p>');
                            if($.inArray(ele.name,Object.keys(data.errors)) !== -1){
                               for(var n=0;n <= 10;n++){
                                   if( Object.keys(data.errors)[n] === ele.name ){
                                        $(this).siblings('p.text-error').text(data.errors[ele.name][0]);
                                   }
                               }
                            }
                    });
                }
                else{
                    $('#contact-loader').slideDown();
                    window.location.href = "https://localhost/app4legal-wp/success/";
                }
           },
           error:function(){
           }
        });
    });
    
    
    $('#request-quote-button').on('click',function(e){
        e.preventDefault();
        var firstname = $('#firstName').val(),
            lastname = $('#lastName').val(),
            email = $('#email').val(),
            organization = $('#organization').val(),
            phone = $('#phone').val(),
            country = $('#country').val(),
            noOfUsers = $('#noOfUsers').val(),
            installation = $('#installation').val(),
            training = $('#training').val(),
            captcha = $('.g-recaptcha-response').val(),
            message = $('#message').val();
        var formdata = $('#contact-form').serialize();
        $.ajax({
           url : apl4_signup.ajax_url,
           type: 'POST',
           cache:false,
           data:{ firstName : firstname, lastName : lastname, email : email, organization : organization,captcha:captcha, phone: phone, country: country, noOfUsers: noOfUsers, training: training, installation: installation, message:message, action : 'manipulate_request_for_quote' },
           beforeSend:function(){
               $('#contact-loader').slideDown();
           },
           success:function( data ){
               $('p.text-error').remove();
               if(data.errors){
                    $('#contact-loader').slideUp();
                    $('#contact-form').find('input,textarea,select').each(function(index,ele){
                        $(this).after('<p class="text-error" style="color:crimson;margin-top:-5px;"> </p>');
                            if($.inArray(ele.name,Object.keys(data.errors)) !== -1){
                               for(var n=0;n <= 10;n++){
                                   if( Object.keys(data.errors)[n] === ele.name ){
                                        $(this).siblings('p.text-error').text(data.errors[ele.name][0]);
                                   }
                               }
                            }
                    });
                }
                else{
                    $('#contact-loader').slideDown();
                    window.location.href = "https://localhost/app4legal-wp/success/";
                }
           },
           error:function(){
           }
        });
    });
});


function validateEmail(email) {
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email.toLowerCase());
}
function showModal(message, type){
    if(type == 'error'){ var dialogStyle = 'modal-error'}else{ dialogStyle = 'modal-success' }
    var modal =   '<div class="modal fade ap4l-subscription-modal-lg" tabindex="10">';
        modal +=  '<div class="modal-dialog modal-lg" role="document">';
        modal +=  '<div class="modal-content '+ dialogStyle +'"><p>' + message;
        modal +=  '</p></div></div></div>';
    return modal;    
}