<?php
/**
 * plugin Name: app4legal-integrations
 * description: plugin will be responsible for handlining all the user forms (Request For Demo - contact us - etc..). 
 * version: 1.0
 * author:infosysta
 * author URI: http://infosysta.com
 * Text domain: app4legal 
 *  
 */

require 'vendor/autoload.php';
require  'request.php';
//require 'dbTables.php';
require 'Init.php';


//add_action('wp_ajax_manipulate_contact_us','app4legal_contact_us');
//add_action('wp_ajax_nopriv_manipulate_contact_us','app4legal_contact_us');
//
//add_action('wp_ajax_manipulate_request_for_quote','app4legal_request_for_quote');
//add_action('wp_ajax_nopriv_manipulate_request_for_quote','app4legal_request_for_quote');
//
//add_action('wp_ajax_manipulate_subscribe','app4legal_subscribe');
//add_action('wp_ajax_nopriv_manipulate_subscribe','app4legal_subscribe');

//register_activation_hook(__FILE__, 'app4legal_tables_install');

Init::route('Utility->updateBaseUrl', 'update_siteurl');

